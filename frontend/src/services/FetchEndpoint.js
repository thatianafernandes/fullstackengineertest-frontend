export function buildUrl(parameters) {
    let base = "http://localhost:5000"
    let alarms = "/alarms"
  
    let objUrl = {
        url : base + alarms,
        firstParameter : true
      }
   
    if (notNullOrEmpty(parameters.outcome)) {    
      objUrl = addParameter(objUrl, "outcome", (parameters.outcome === false)? "false" : "true");
    }
  
    if (notNullOrEmpty(parameters.location)) {    
      objUrl = addParameter(objUrl, "location", parameters.location);
    }  
  
    if (notNullOrEmpty(parameters.page[0])) {    
      objUrl = addParameter(objUrl, "pageNumber", parameters.page[0]);
    }  
  
    if (notNullOrEmpty(parameters.page[1])) {    
      objUrl = addParameter(objUrl, "pageSize", parameters.page[1]);
    }  
  
    if (notNullOrEmpty(parameters.timestamps[0])) {    
      objUrl = addParameter(objUrl, "start", parameters.timestamps[0]);
    }  
  
    if (notNullOrEmpty(parameters.timestamps[1])) {    
      objUrl = addParameter(objUrl, "end", parameters.timestamps[1]);
    }  

    console.log(objUrl.url);
    return objUrl.url;
}

function notNullOrEmpty(param) {
    return param !== null && param !== "";
}

function parameterSeparator(firstParameter) {    
    return (firstParameter === true)? "?" : "&";
}

function addParameter(objUrl, nameParameter, value) {
    objUrl.url += parameterSeparator(objUrl.firstParameter) + nameParameter + "=" +value;
    objUrl.firstParameter = false;    
    return objUrl;
}